# Schenkler Sensor

This repository contains the code and the ROS node for the Schenkler(TM) optical ground speed sensor. 

## Current State

* very WIP
* prototype lib should work
* python prototype and tests should also work
* ROS node untested and not yet documented

TODO:
* use the algorithm I mentioned in the presentation (TM_CCOEFF_NORMED)
* fix speed offset bug, when car is not moving (caused by wrong offsets and smaller templates in `matchFrames()`)

## Getting Started

Execute the setup script and pray that it works:
```
sh ./setup.sh
```

This script will extract the images from the test data videos, create a 'broken' version with a vignette filter. This script will also build the source code and put a symlink into the `prototype` folder. 

If everything worked, all tests in the `customcv.ipynb` notebook should pass and the output graph look somewhat plausible. 

## Getting to now the algorithm

The `prototype` folder contains a python prototype inside the `prototype.ipynb` notebook (if you want to play around with the basics) and a 'Unit Test' Notebook (`customcv.ipynb`) which tests the compiled C version against the python reference. Use this to make sure, that the algorithm still works, after you made changes to the C source code. 

## Folder Structure

### [config](config)

Contains the launch configurations for the Schenkler ROS node (resolution, ROI, ...). 

### [docs](docs)

Contains documentation and the ARWo presentation. 

### [data](data)

Contains test data.

### [img](img)

Contains a few test images, both with and without a vignette filter. 

### [src](src) [include](include)

Source code and header files.

### [launch](launch)

Contains launch file for Schenkler ROS node. 

### [lib](lib)

Contains a `CMakeLists.txt` for the Schenkler library. 