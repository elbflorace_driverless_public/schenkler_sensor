import cv2
import numpy as np
import os
import matplotlib.pyplot as plt
import time
from tqdm import tqdm

def break_images(img):
    rows, cols = img.shape
   
    # generating vignette mask using Gaussian
    # resultant_kernels
    X_resultant_kernel = cv2.getGaussianKernel(cols,200)
    Y_resultant_kernel = cv2.getGaussianKernel(rows,200)
    
    #generating resultant_kernel matrix
    resultant_kernel = Y_resultant_kernel * X_resultant_kernel.T
    
    #creating mask and normalising by using np.linalg
    # function
    mask = 255 * resultant_kernel / np.linalg.norm(resultant_kernel)

    return img * mask

# load all images files from selected directory in ascending order and convert them to grayscale
img_dir = "video_k15"
imgs_names = [i for i in os.listdir(img_dir) if i[-4:] == ".png" ]
imgs_names = sorted(imgs_names)
raw_imgs = [cv2.cvtColor(cv2.imread(img_dir + "/" + i), cv2.COLOR_BGR2GRAY) for i in imgs_names if int(i[-9:-4]) in range(0, 20000)]

for i in tqdm(range(len(raw_imgs))):
    img = break_images(raw_imgs[i])
    cv2.imwrite("videok15broken/" + imgs_names[i], img)