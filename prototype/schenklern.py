from ctypes import CDLL, POINTER, c_uint, c_float, c_ubyte, c_int, byref
from numpy.ctypeslib import ndpointer
import numpy as np

lib = CDLL("./libschenklern.so")

def scaledown(img, factor):
    # create input and outputs
    height, width = img.shape
    img = np.ascontiguousarray(img)
    img_small = np.ascontiguousarray(np.zeros((height // factor, width // factor)), dtype=np.uint8)
    img_small_flt = np.ascontiguousarray(np.zeros((height // factor, width // factor)), dtype=np.float32)

    # define function signature
    lib.scale_down_c.argtypes = [ndpointer(c_ubyte, flags="C_CONTIGUOUS"), ndpointer(c_float, flags="C_CONTIGUOUS"), ndpointer(c_ubyte, flags="C_CONTIGUOUS"), c_uint, c_uint, c_uint]
    lib.scale_down_c.restype = None

    # execute function
    lib.scale_down_c(img_small, img_small_flt, img, int(width), int(height), factor)

    return img_small
    
def matchTemplate(img, tmp):
    height, width = img.shape
    tmp_height, tmp_width = tmp.shape
    out_w = width - tmp_width + 1
    out_h = height - tmp_height + 1

    img = np.ascontiguousarray(img)
    tmp = np.ascontiguousarray(tmp)
    match = np.ascontiguousarray(np.zeros((out_h, out_w)), dtype=np.float32)

    # define function signature
    lib.matchTemplate_c.argtypes = [ndpointer(c_float, flags="C_CONTIGUOUS"), ndpointer(c_ubyte, flags="C_CONTIGUOUS"), c_uint, c_uint, ndpointer(c_ubyte, flags="C_CONTIGUOUS"), c_uint, c_uint]
    lib.matchTemplate_c.restype = None
    
    # execute function
    lib.matchTemplate_c(match, img, width, height, tmp, tmp_width, tmp_height)

    return match

def match_frames(img, previous_img, template_width, template_height, offset_x, offset_y):
    # create input and outputs
    height, width = img.shape
    img = np.ascontiguousarray(img)
    prev = np.ascontiguousarray(previous_img)

    dx = c_int()
    dy = c_int()
    snr = c_float()
    best = c_float()

    # define function signature
    lib.match_frames_c.argtypes = [POINTER(c_int), POINTER(c_int), POINTER(c_float), POINTER(c_float), ndpointer(c_ubyte, flags="C_CONTIGUOUS"), c_uint, c_uint, ndpointer(c_ubyte, flags="C_CONTIGUOUS"), c_uint, c_uint, c_int, c_int]
    lib.match_frames_c.restype = None

    # execute function
    lib.match_frames_c(byref(dx), byref(dy), byref(best), byref(snr), img, width, height, prev, template_width, template_height, offset_x, offset_y)

    return dx.value, dy.value, best.value, snr.value

def img_flow(img, previous_img, template_width, template_height):
    # create input and outputs
    height, width = img.shape
    img = np.ascontiguousarray(img)
    previous_img = np.ascontiguousarray(previous_img)

    dx = c_int()
    dy = c_int()
    snr = c_float()
    best = c_float()

    # define function signature
    lib.img_flow_c.argtypes = [POINTER(c_int), POINTER(c_int), POINTER(c_float), POINTER(c_float), ndpointer(c_ubyte, flags="C_CONTIGUOUS"), c_uint, c_uint, ndpointer(c_ubyte, flags="C_CONTIGUOUS"), c_uint, c_uint]
    lib.img_flow_c.restype = None

    # execute function
    lib.img_flow_c(byref(dx), byref(dy), byref(best), byref(snr), img, width, height, previous_img, template_width, template_height)

    return dx.value, dy.value, best.value, snr.value

def in_range(lower, value, upper):
    # define function signature
    lib.in_range.argtypes = [c_float, c_float, c_float]
    lib.in_range.restype = c_int

    # execute function
    return lib.in_range(lower, value, upper)

def check_frameskip(x, prev_x):
    # define function signature
    lib.check_frameskip.argtypes = [c_float, c_float]
    lib.check_frameskip.restype = c_int 

    # execute function
    return lib.check_frameskip(x, prev_x)
    
def remove_outliers(u, v, prev_u, prev_v, best, snr):
    # allocate inputs and outputs
    u_out = c_int()
    v_out = c_int()

    # define function signature
    lib.remove_outliers.argtypes = [POINTER(c_int), POINTER(c_int), c_int, c_int, c_int, c_int, c_float, c_float]
    lib.remove_outliers.restype = c_int

    # execute function
    status = lib.remove_outliers(byref(u_out), byref(v_out), u, v, prev_u, prev_v, best, snr)

    return status, u_out.value, v_out.value