#ifndef SCHENKLERN_H_
#define SCHENKLERN_H_

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief  Scales down images
 * @note   Scales down image by given factor
 * @param  img_small: small output image
 * @param  img_small_flt: small float output image (used to store intermediate result)
 * @param  img: input image
 * @param  width: input image width
 * @param  height: input image height
 * @param  factor: scale factor to scale down with
 * @retval None
 */
void scale_down_c(unsigned char* img_small, float* img_small_flt, const unsigned char* img, unsigned int height, unsigned int factor);

/**
 * @brief  Template matching (C version)
 * @note   Matches template over image
 * @param  dx: the match image to write into (needs to be pre allocated with size of W-w_t+1 x H-h_t+1)
 * @param  img: input image
 * @param  width: input image width
 * @param  height: input image height
 * @param  tmp: template
 * @param  template_width: width of the template to select
 * @param  template_height: height of the template to select
 * @retval None
 */
void matchTemplate_c(unsigned char* match, const unsigned char* img, unsigned int width, unsigned int height, const unsigned char* tmp, unsigned int tmp_width, unsigned int tmp_height);

/**
 * @brief  Match frames (C version)
 * @note   Matches frames and returns offset along x and y axis alongside quality information
 * @param  dx: x offset
 * @param  dy: y offset
 * @param  best: best match value
 * @param  snr: signal to noise ratio
 * @param  img: input image
 * @param  width: input image width
 * @param  height: input image height
 * @param  previous_img: previous image to match against
 * @param  template_width: width of the template to select
 * @param  template_height: height of the template to select
 * @param  offset_x: x offset of template
 * @param  offset_y: y offset of template
 * @retval None
 */
void match_frames_c(int* dx, int* dy, float* best, float* snr, const unsigned char* img, unsigned int width, unsigned int height, const unsigned char* previous_img, unsigned int template_width, unsigned int template_height, int offset_x, int offset_y);

/**
 * @brief  Calculates img flow direction (C version)
 * @note   Calculates image flow direction of each corner and returns best result
 * @param  dx: x offset
 * @param  dy: y offset
 * @param  best: best match value
 * @param  snr: signal to noise ratio
 * @param  img: input image
 * @param  width: input image width
 * @param  height: input image height
 * @param  previous_img: previous image to match against
 * @param  template_width: width of the templates in the corners
 * @param  template_height: height of the templates in the corners
 * @retval None
 */
void img_flow_c(int* dx, int* dy, float* best, float* snr, const unsigned char* img, unsigned int width, unsigned int height, const unsigned char* previous_img, unsigned int template_width, unsigned int template_height);

/**
 * @brief  Tests range
 * @note   Checks if the value is within an upper and a lower bound
 * @param  lower: lower bound
 * @param  value: value to test
 * @param  upper: upper bound
 * @retval 1 if the value is in range, 0 otherwise
 */
int in_range(float lower, float value, float upper);

/**
 * @brief  Checks for frame skips
 * @note   Checks if a frame is skipped or other quality factors hint at a wrong match
 * @param  x: movement offset
 * @param  prev_x: previous movement offset
 * @retval 0 = plausible, 1 = skipped frame / double speed, 2 = bad SNR, 3 = bad best match, 4 = unrealistic movement
 */
int check_frameskip(float x, float prev_x);

/**
 * @brief  Removes outliers
 * @note   Filters outliers using the check frame method and estimates values if the measurement is unreliable
 * @param  u_out: x offset output
 * @param  v_out: y offset output
 * @param  u: x offset inptu
 * @param  v: y offset input
 * @param  prev_u: previous x offset 
 * @param  prev_v: previous y offset
 * @param  best: value of best match
 * @param  snr: signal to noise ratio
 * @retval 0 = plausible, 1 = skipped frame / double speed, 2 = bad SNR, 3 = bad best match, 4 = unrealistic movement
 */
int remove_outliers(int* u_out, int* v_out, int u, int v, int prev_u, int prev_v, float best, float snr);

#ifdef __cplusplus
}
#endif

#endif /* SCHENKLERN_H_ */