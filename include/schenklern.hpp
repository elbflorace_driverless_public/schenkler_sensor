#ifndef SCHENKLERN_HPP_
#define SCHENKLERN_HPP_

#include "opencv2/imgproc.hpp"

/**
 * @brief  Match frames
 * @note   Matches frames and returns offset along x and y axis alongside quality information
 * @param  dx: x offset
 * @param  dy: y offset
 * @param  best: best match value
 * @param  snr: signal to noise ratio
 * @param  img: input image
 * @param  previous_img: previous image to match against
 * @param  template_width: width of the template to select
 * @param  template_height: height of the template to select
 * @param  offset_x: x offset of template
 * @param  offset_y: y offset of template
 * @retval None
 */
void match_frames(int* dx, int* dy, float* best, float* snr, const cv::Mat img, const cv::Mat previous_img, unsigned int template_width, unsigned int template_height, int offset_x, int offset_y);

/**
 * @brief  Calculates img flow direction
 * @note   Calculates image flow direction of each corner and returns best result
 * @param  dx: x offset
 * @param  dy: y offset
 * @param  best: best match value
 * @param  snr: signal to noise ratio
 * @param  img: input image
 * @param  previous_img: previous image to match against
 * @param  template_width: width of the templates in the corners
 * @param  template_height: height of the templates in the corners
 * @retval None
 */
void img_flow(int* dx, int* dy, float* best, float* snr, const cv::Mat img, const cv::Mat previous_img, unsigned int template_width, unsigned int template_height);

// disable C++ name mangling by creating pure C functions
// we need this to be able to access the functions in python
extern "C" {

/**
 * @brief  Tests range
 * @note   Checks if the value is within an upper and a lower bound
 * @param  lower: lower bound
 * @param  value: value to test
 * @param  upper: upper bound
 * @retval 1 if the value is in range, 0 otherwise
 */
int in_range(float lower, float value, float upper);

/**
 * @brief  Checks for frame skips
 * @note   Checks if a frame is skipped or other quality factors hint at a wrong match
 * @param  x: movement offset
 * @param  prev_x: previous movement offset
 * @retval 0 = plausible, 1 = skipped frame / double speed, 2 = bad SNR, 3 = bad best match, 4 = unrealistic movement
 */
int check_frameskip(float x, float prev_x);

/**
 * @brief  Removes outliers
 * @note   Filters outliers using the check frame method and estimates values if the measurement is unreliable
 * @param  u_out: x offset output
 * @param  v_out: y offset output
 * @param  u: x offset inptu
 * @param  v: y offset input
 * @param  prev_u: previous x offset 
 * @param  prev_v: previous y offset
 * @param  best: value of best match
 * @param  snr: signal to noise ratio
 * @retval 0 = plausible, 1 = skipped frame / double speed, 2 = bad SNR, 3 = bad best match, 4 = unrealistic movement
 */
int remove_outliers(int* u_out, int* v_out, int u, int v, int prev_u, int prev_v, float best, float snr);

}

#endif /* SCHENKLERN_HPP_ */