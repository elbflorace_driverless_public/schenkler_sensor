#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <geometry_msgs/TwistWithCovarianceStamped.h>
#include <cstdint>

#include "schenklern.hpp"

cv::Mat previous_img;
ros::Time prev_time;
ros::Publisher velo_pub;
ros::NodeHandle *nh;

int template_width = 64;
int template_height = 64;
int offset_x = (320 - template_width) * 9 / 10;
int offset_y = (256 - template_height) / 2;
double m_per_pixel = 7.4 / 100. / 256;
int loop_counter = 0;
int update_rate = 16;

void imageCallback(const sensor_msgs::ImageConstPtr& msg)
{
    // calculate displacement
    cv::Mat img = cv_bridge::toCvShare(msg, "mono8")->image;
    // cv::Mat img_raw = cv_bridge::toCvShare(msg, "mono8")->image;
    // possibly downsample in the future
    // cv::Mat img = img_raw;
    // resize(img_raw, img, cv::Size(320, 256), scalar, scalar, cv::INTER_AREA);
    ros::Time current_time = msg->header.stamp;

    int dx, dy;
    float best, snr;

    if(nh && loop_counter % update_rate == 0) {
        nh->param<int>("template_width", template_width, 64);
        nh->param<int>("template_height", template_height, 64);
        nh->param<int>("offset_x", offset_x, (img.cols - template_width) * 9 / 10);
        nh->param<int>("offset_y", offset_y, (img.rows - template_height) / 2);
        nh->param<double>("m_per_pixel", m_per_pixel, 7.4 / 100. / img.rows);

        loop_counter = 1;
    }

    if(previous_img.cols && previous_img.rows) {
        match_frames(&dx, &dy, &best, &snr, img, previous_img, template_width, template_height, offset_x, offset_y);

        // calculate speed
        double duration = (current_time - prev_time).toSec();
        double vx = dx * m_per_pixel / duration;
        double vy = dy * m_per_pixel / duration;

        // publish velocity
        geometry_msgs::TwistWithCovarianceStamped velo;

        velo.header = msg->header;
        velo.twist.twist.linear.x = vx;
        velo.twist.twist.linear.y = vy;
        velo.twist.twist.linear.z = 0.;//best;

        velo.twist.twist.angular.x = 0.;//dx;
        velo.twist.twist.angular.y = 0.;//dy;
        velo.twist.twist.angular.z = 0.;//snr;

        velo.twist.covariance = {0};
        velo.twist.covariance[0] = (best + snr) * (best + snr);
        velo.twist.covariance[1 * 6 + 1] = (best + snr) * (best + snr);

        // publish 
        velo_pub.publish(velo);
        ++loop_counter;
    }

    // save temp state
    previous_img = img.clone();
    prev_time = current_time;
}

int main(int argc, char **argv)
{
    // init node
    ros::init(argc, argv, "schenkler");
    ros::NodeHandle _nh;
    nh = &_nh;
    // init image subscriber and velocity publisher and spin
    image_transport::ImageTransport it(_nh);
    std::string camera_topic_name;
    _nh.param<std::string>("camera_topic_name", camera_topic_name, "/schenkler_cam/image_raw");
    _nh.param<int>("update_rate", update_rate, 16);
    image_transport::Subscriber sub = it.subscribe(camera_topic_name, 1, imageCallback);
    std::string schenkler_topic_name;
    _nh.param<std::string>("schenkler_topic_name", schenkler_topic_name, "/schenkler/velocity");
    velo_pub = _nh.advertise<geometry_msgs::TwistWithCovarianceStamped>(schenkler_topic_name, 1000);
    ros::spin();
}