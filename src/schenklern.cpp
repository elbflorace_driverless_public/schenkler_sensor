#include "schenklern.hpp"

#include "opencv2/imgproc.hpp"

#include <cstring>
#include <cstdlib>
#include <numeric>

using namespace cv;

// #define CCOEFF

unsigned int extract_2powx(unsigned int n) {
    float f = n;
    unsigned int* i = (unsigned int*)&f;
    *i &= (0xFF << 23);
    return (unsigned int)f;
}

void match_frames(int* dx, int* dy, float* best, float* snr, const Mat img, const Mat previous_img, unsigned int template_width, unsigned int template_height, int offset_x, int offset_y){
    /* allocate loop variables */
    int divisions = std::max(int(extract_2powx(std::min(template_width, template_height)) >> 3), 1);
    Mat small_img;
    Mat tmp = previous_img(Rect(offset_x, offset_y, template_width, template_height));
    Mat small_tmp;
    Mat match;

    /* initialize output */
    *dx = 0;
    *dy = 0;

    /* scale down image and template */
    resize(img, small_img, Size(), 1.f / divisions, 1.f / divisions, INTER_AREA);
    resize(tmp, small_tmp, Size(), 1.f / divisions, 1.f / divisions, INTER_AREA);

    /* first match */
#ifndef CCOEFF
    matchTemplate(small_img, small_tmp, match, TM_SQDIFF_NORMED);
#else
    matchTemplate(small_img, small_tmp, match, TM_CCOEFF_NORMED);
#endif

    /* find speed */
    double minval, maxval;
    Point minloc, maxloc;
    minMaxLoc(match, &minval, &maxval, &minloc, &maxloc, Mat() );
#ifndef CCOEFF
    *dx = minloc.x;
    *dy = minloc.y;
    *best = minval;
#else
    *dx = maxloc.x;
    *dy = maxloc.y;
    *best = maxval;
#endif
    float mean = std::accumulate(match.begin<float>(), match.end<float>(), 0.f) / (match.cols * match.rows);

    /* iterate over remaining steps */
    divisions /= 2;
    while(divisions >= 1) {
        /* scale down image and template */
        resize(img, small_img, Size(), 1.f / divisions, 1.f / divisions, INTER_AREA);
        resize(tmp, small_tmp, Size(), 1.f / divisions, 1.f / divisions, INTER_AREA);

        *dx *= 2;
        *dy *= 2;

        /* cut out image around previous match */
        int x_min = std::max(*dx - 1, 0);
        int x_max = std::min(*dx + 2 + small_tmp.cols, small_img.cols - 1);
        int y_min = std::max(*dy - 1, 0);
        int y_max = std::min(*dy + 2 + small_tmp.rows, small_img.rows - 1);
        int small_width = std::max(x_max - x_min, 0);
        int small_height = std::max(y_max - y_min, 0);

        /* ensure that size is big enough for template matching */
        if((small_height < small_tmp.rows) || (small_width < small_tmp.cols)) {
            *best = 1.f;
            *snr = 1.f / mean;
            return;
        } 

        /* match */
#ifndef CCOEFF
        matchTemplate(small_img(Rect(x_min, y_min, small_width, small_height)), small_tmp, match, TM_SQDIFF_NORMED);
#else
        matchTemplate(small_img(Rect(x_min, y_min, small_width, small_height)), small_tmp, match, TM_CCOEFF_NORMED);
#endif
        minMaxLoc(match, &minval, &maxval, &minloc, &maxloc, Mat() );
        
        /* update displacement */
#ifndef CCOEFF
        *dx += minloc.x - 1;
        *dy += minloc.y - 1;
        *best = minval;
#else
        *dx += maxloc.x - 1;
        *dy += maxloc.y - 1;
        *best = maxval;
#endif
        
        /* update size variables */
        divisions /= 2;
    }

    if(mean <= 0) {
        mean = 0.0001f;
    }

    *dx -= offset_x;
    *dy -= offset_y;
    *snr = *best / mean;
}

void img_flow(int* dx, int* dy, float* best, float* snr, const Mat img, const Mat previous_img, unsigned int template_width, unsigned int template_height) {
    /* pre-calculate possible template offsets */
    int width = img.cols;
    int height = img.rows;
    int offset_x = width - template_width;
    int offset_y = height - template_height;
    int dxs[4] = {0}, dys[4] = {0};
    float bests[4] = {0.f}, snrs[4] = {0.f};
    float best_match;
    unsigned int i;

    /* match all corners */
    match_frames(&dxs[0], &dys[0], &bests[0], &snrs[0], img, previous_img, template_width, template_height, 0, 0);
    match_frames(&dxs[1], &dys[1], &bests[1], &snrs[1], img, previous_img, template_width, template_height, offset_x, 0);
    match_frames(&dxs[2], &dys[2], &bests[2], &snrs[2], img, previous_img, template_width, template_height, 0, offset_y);
    match_frames(&dxs[3], &dys[3], &bests[3], &snrs[3], img, previous_img, template_width, template_height, offset_x, offset_y);

    /* select best match */
#ifndef CCOEFF
    best_match = std::min(std::min(bests[0], bests[1]), std::min(bests[2], bests[3]));
#else
    best_match = std::max(std::max(bests[0], bests[1]), std::max(bests[2], bests[3]));
#endif
    for(i = 0; i < 4; ++i) {
        if(bests[i] == best_match) {
            *dx = dxs[i];
            *dy = dys[i];
            *best = bests[i];
            *snr = snrs[i];
            break;
        }
    }
}

// disable C++ name mangling by creating pure C functions
// we need this to be able to access the functions in python
extern "C" {

void scale_down_c(unsigned char* img_small, float* img_small_flt, const unsigned char* img, unsigned int width, unsigned int height, unsigned int factor) {
    Mat cv_img((int)height, (int)width, CV_8UC1, (void*)img);
    Mat cv_result;
    // ignore img_small (because its left over from an old custom resize implementation)

    resize(cv_img, cv_result, Size(), 1.f / factor, 1.f / factor, INTER_AREA);

    // copy image back (because CV seems to reallocate the result image and therefore store the result somewhere else)
    memcpy(img_small, cv_result.data, cv_result.rows * cv_result.cols * sizeof(img_small[0]));
}

void matchTemplate_c(float* match, const unsigned char* img, unsigned int width, unsigned int height, const unsigned char* tmp, unsigned int tmp_width, unsigned int tmp_height) {
    Mat cv_img((int)height, (int)width, CV_8UC1, (void*)img);
    Mat cv_tmp((int)tmp_height, (int)tmp_width, CV_8UC1, (void*)tmp);
    Mat cv_result;

    matchTemplate(cv_img, cv_tmp, cv_result, TM_SQDIFF_NORMED);

    // copy image back (because CV seems to reallocate the result image and therefore store the result somewhere else)
    memcpy(match, cv_result.data, cv_result.rows * cv_result.cols * sizeof(match[0]));
}

void match_frames_c(int* dx, int* dy, float* best, float* snr, const unsigned char* img, unsigned int width, unsigned int height, const unsigned char* previous_img, unsigned int template_width, unsigned int template_height, int offset_x, int offset_y) {
    Mat cv_img((int)height, (int)width, CV_8UC1, (void*)img);
    Mat cv_previous_img((int)height, (int)width, CV_8UC1, (void*)previous_img);

    match_frames(dx, dy, best, snr, cv_img, cv_previous_img, template_width, template_height, offset_x, offset_y);
}

void img_flow_c(int* dx, int* dy, float* best, float* snr, const unsigned char* img, unsigned int width, unsigned int height, const unsigned char* previous_img, unsigned int template_width, unsigned int template_height) {
    Mat cv_img((int)height, (int)width, CV_8UC1, (void*)img);
    Mat cv_previous_img((int)height, (int)width, CV_8UC1, (void*)previous_img);

    img_flow(dx, dy, best, snr, cv_img, cv_previous_img, template_width, template_height);
}

int in_range(float lower, float value, float upper) {
    float lower_bound = std::min(lower, upper);
    float upper_bound = std::max(lower, upper);
    return (lower_bound <= value) && (value <= upper_bound);
}

int check_frameskip(float x, float prev_x) {
    /* only act when going above minimum speed */
    if(std::abs(prev_x) < 10. && std::abs(x) < 10.) {
        return 0;
    }

    /* check for correct value */
    if(in_range(prev_x * 0.8f, x, prev_x * 1.2f)) {
        return 0;

    /* check for frame skip */
    } else if(in_range(prev_x * 1.8f, x, prev_x * 2.2f)) {
        return 1;

    /* check for physically implausible movement */
    } else {
        return 4;
    }
}

int remove_outliers(int* u_out, int* v_out, int u, int v, int prev_u, int prev_v, float best, float snr) {
    int frameskip_u = check_frameskip(u, prev_u);
    int frameskip_v = check_frameskip(v, prev_v);

    /* check for bad match */
    if(best > 0.2) {
        *u_out = prev_u;
        *v_out = prev_v;
        return 3;
    }

    /* check signal to noise ratio */
    if(snr > 0.2) {
        *u_out = prev_u;
        *v_out = prev_v;
        return 2;
    }

    if(frameskip_u == 4 || frameskip_v == 4) {
        *u_out = prev_u;
        *v_out = prev_v;
        return 4;
    }

    if(frameskip_u == 1 || frameskip_v == 1) {
        *u_out = u / 2;
        *v_out = v / 2;
        return 1;
    }

    *u_out = u;
    *v_out = v;
    return 0;
}

}