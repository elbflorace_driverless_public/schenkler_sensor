#!/bin/sh

# extract images from video
cd data
mkdir videok7
ffmpeg -i k7.h264 -vf fps=25 videok7/videok7_%5d.png
mkdir video_k15
ffmpeg -i k15.h264 -vf fps=25 video_k15/videok15_%5d.png
mkdir videok15broken
python3 ../prototype/break_images.py

# go to lib directory and create build directory
cd ../lib
mkdir build
cd build

# configure and build
cmake ..
make

# create symlink in prototype directory
cd ../../prototype/
ln -s ../lib/build/libschenklern.so libschenklern.so